import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueCountdown from '@chenfengyuan/vue-countdown';
import Sticky from 'vue-sticky-directive';

Vue.config.productionTip = false
Vue.component(VueCountdown.name, VueCountdown);
Vue.use(Sticky)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

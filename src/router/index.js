/* eslint-disable */
import Vue from 'vue'
import VueRouter from 'vue-router'
import authenticate from "./../auth/authenticate";

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import("../views/home/home.vue"),
  },
  {
    path: '/help',
    name: 'Help',
    component: () => import("../views/help/faq.vue"),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import("../views/session/login.vue"),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import("../views/session/register.vue"),
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: () => import("../views/session/resetPassword.vue"),
  },
  {
    path: '/forget-password',
    name: 'Forget',
    component: () => import("../views/session/forgetPassword.vue"),
  },
  {
    path: '/confirm-account',
    name: 'AccountAcc',
    component: () => import("../views/session/account-confirm.vue"),
  },
  {
    path: '/movie',
    name: 'Movie',
    beforeEnter: authenticate,
    component: () => import("../views/movie/show.vue"),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import("../views/test/test.vue"),
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    beforeEnter: authenticate,
    component: () => import("../views/dashboard/index.vue"),
  },
  {
    path: '/profile',
    name: 'Profile',
    beforeEnter: authenticate,
    component: () => import("../views/dashboard/profile.vue"),
  },
  {
    path: '/referal',
    name: 'Referal',
    beforeEnter: authenticate,
    component: () => import("../views/dashboard/referal.vue"),
  },
  {
    path: '/settings',
    name: 'Settings',
    beforeEnter: authenticate,
    component: () => import("../views/dashboard/update_password.vue"),
  },
  {
    path: '/transaction',
    name: 'Transaction',
    beforeEnter: authenticate,
    component: () => import("../views/dashboard/history_payment.vue"),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0};
  }
})

export default router

import AuthService from "@/services/AuthService"

export default {
  state: {
    loggedInUser:
      localStorage.getItem("userInfo") != null
        ? localStorage.getItem("userInfo")
        : null,
    loading: false,
    error: null
  },
  getters: {
    loggedInUser: state => state.loggedInUser,
    loading: state => state.loading,
    error: state => state.error
  },
  mutations: {
    setUser(state, data) {
      state.loggedInUser = data;
      state.loading = false;
      state.error = null;
    },
    setLogout(state) {
      state.loggedInUser = null;
      state.loading = false;
      state.error = null;
      // this.$router.go("/");
    },
    setLoading(state, data) {
      state.loading = data;
      state.error = null;
    },
    setError(state, data) {
      state.error = data;
      state.loggedInUser = null;
      state.loading = false;
    },
    clearError(state) {
      state.error = null;
    }
  },
  actions: {
    async login({commit}, data) {
      commit("clearError");
      try {
        const response = await AuthService.login(data)
        if(response.data.success == true) {
          const user     = response.data.detailUser

          localStorage.setItem("access-token", user.token);
          localStorage.setItem("userInfo", JSON.stringify(user));
        
          console.log('true')
          commit("setUser", user);
        } else {
          localStorage.removeItem("userInfo");
          localStorage.removeItem("access-token");
          localStorage.removeItem("profile");

          commit("setError", response.data.message);
        }
      
      } catch (error) {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("access-token");
        localStorage.removeItem("profile");
        commit("setError", error);
      }
    },
    async resetPassword({commit}, data) {
      commit("clearError");
      try {
        const response = await AuthService.login(data)
        const user     = response.data.detailUser

        localStorage.setItem("access-token", user.token);
        localStorage.setItem("userInfo", JSON.stringify(user));

        commit("setUser", user);
      
      } catch (error) {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("access-token");
        localStorage.removeItem("profile");
        commit("setError", error);
      }
    },
    async signUserUp({commit}, data) {
      commit("setLoading", true);
      commit("clearError");
      try {
        // await AuthService.register(data)
        const response = await AuthService.register(data)
        const user     = response.data.detailUser
        // console.log(response.data);
        if(response.data.success == true) {
          console.log('true')
          commit("setUser", user);
        } else {
          console.log('false')
          localStorage.removeItem("userInfo");
          localStorage.removeItem("access-token");
          localStorage.removeItem("profile");

          commit("setError", response.data.message);
        }
        
      } catch (error) {
        
        console.log('error')
        
        localStorage.removeItem("userInfo");
        localStorage.removeItem("access-token");
        localStorage.removeItem("profile");

        // commit("setError", error);
      }
    },
  }
};
